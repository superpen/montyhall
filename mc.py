import random


class Mc:
    def __init__(self):
        self.game_sequence = []
        self.opened_door = None
        self.price_placement = None

    def design_game(self, tries):
        for i in range(tries):
            self.game_sequence.insert(i, [0, 0, 0])
            price = random.randint(0, 2)
            self.game_sequence[i][price] = 1

    def open_door(self, sequence, choice):
        self.price_placement = sequence.index(1)
        ocupide_doors = [self.price_placement, choice]

        if 0 not in ocupide_doors:
            self.opened_door = 0
        elif 1 not in ocupide_doors:
            self.opened_door = 1
        elif 2 not in ocupide_doors:
            self.opened_door = 2

'''
monty = Mc()
monty.design_game(3)
#print(monty.game_sequence)
for i in monty.game_sequence:
    print("sequence", i)
    monty.open_door(i, random.randint(0, 2))
    print("opened door", monty.opened_door)
'''