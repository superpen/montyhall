from mc import Mc
from player import Player


print("Welcome to Monty Hall Paradox experiment.")
rounds = int(input("Please enter integer number of rounds you want to make: "))

monty = Mc()
monty.design_game(rounds)

Bull = Player()
Smart = Player()

for sequence in monty.game_sequence:
    Bull.vote()
    monty.open_door(sequence, Bull.choice)
    if Bull.choice == monty.price_placement:
        Bull.won()
    else:
        Bull.lost()

    Smart.vote()
    monty.open_door(sequence, Smart.choice)
    Smart.vote_again(monty.opened_door)
    if Smart.choice == monty.price_placement:
        Smart.won()
    else:
        Smart.lost()

print("here are results of Bull strategy. The bull won - ",
      Bull.history['victory'], " times.", "And lost - ", Bull.history['loss'],
      " times.")
print("here are results of Smart strategy. The smart won - ",
      Smart.history['victory'], " times.",
      "And lost - ", Smart.history['loss'], " times.")
