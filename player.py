import random


class Player():
    def __init__(self):
        self.history = {
            'victory': 0,
            'loss': 0
        }
        self.choice = None
        self.used_doors = None

    def vote(self):
        self.choice = random.randint(0, 2)

    def vote_again(self, opened_door):
        self.used_doors = [self.choice, opened_door]
        if 0 not in self.used_doors:
            self.choice = 0
        elif 1 not in self.used_doors:
            self.choice = 1
        elif 2 not in self.used_doors:
            self.choice = 2

    def won(self):
        self.history['victory'] += 1

    def lost(self):
        self.history['loss'] += 1

"""
playerA = Player()
playerA.vote()
print("First choice", playerA.choice)
playerA.vote_again(1)
playerA.won()
playerA.lost()
print(playerA.history)
print(playerA.used_doors)
print("Second choice", playerA.choice)
"""